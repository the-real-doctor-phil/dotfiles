Hejsan!

Om du läser det här har du antagligen lite problem med att få igång en ny
installation.

Tricket är:

1. Installera de program som ser bra ut i pkg_list.txt (arch-paket de senaste
gångerna iaf).

2. Se till att stow är installerat.

3. git clone --recursive https://the-real-doctor-phil@gitlab.com/the-real-doctor-phil/dotfiles.git
   till en ny mapp dotfiles i din hemkatalog.

4. cd dotfiles && ./install.zsh

5. ???

6. Profit!
