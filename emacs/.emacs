(load-file (let ((coding-system-for-read 'utf-8))
                (shell-command-to-string "agda-mode locate")))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (adwaita)))
 '(package-selected-packages (quote (s yasnippet evil))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; Use shift + arrows to switch window.
(when (fboundp 'windmove-default-keybindings)
  (windmove-default-keybindings))

;; Adds some package-repositories
(require 'package)
(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
                         ("marmalade" . "https://marmalade-repo.org/packages/")
                         ("melpa" . "https://melpa.org/packages/")))

(package-initialize)

;; Snippets
(require 'yasnippet)
(setq yas/triggers-in-field t); Enable nested triggering of snippets
(yas-global-mode 1)

;; Use C-x u to see the undo-tree.
(require 'undo-tree)

;; Vim mode
(setq evil-shift-width 2)
(setq evil-want-fine-undo t)
(require 'evil)
(evil-mode 1)
