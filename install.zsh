#!/usr/bin/env zsh

mkdir -p ~/.config #we do not want all .config stowed!
pkgs=("vim" "git" "nethack" "zsh" "aria2c" "awesome")

for p in $pkgs; do
	stow $p
done
