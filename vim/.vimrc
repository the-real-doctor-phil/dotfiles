"just for safety
set nocompatible

"enable code higlightning
syntax on
set showmatch
set mat=2

"line numbering
set number

"working mouse in terminal
set mouse=a

"indentation with tabs
set shiftwidth=2
set tabstop=2
set shiftround
set expandtab

" Enable filetype plugins
filetype on
filetype plugin on
filetype indent on

" Set 7 lines to the cursor - when moving vertically using j/k
set so=7

" Turn on the WiLd menu
set wildmenu
set wildmode=list:longest,full

" Ignore compiled files
set wildignore=*.o,*~,*.pyc

"Always show current position
set ruler

"do not care about case when searching if just searching for lowercase
set ignorecase
set smartcase
"highlight search terms, while typing
set hlsearch
set incsearch
"type :C to clear highlighting
command C nohlsearch

"change the terminals title
set title

"j and k go to wrapped lines as well
nnoremap j gj
nnoremap k gk

"use enter to add a new line after the current line.
nmap <CR> o<Esc>

" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8

" Use Unix as the standard file type
set ffs=unix,dos,mac

"Indentation and line wrapping
set ai "Auto indent
set si "Smart indent
set wrap "Wrap lines

"0 moves to first non-blank character
map 0 ^

"toggle spell checking
map <leader>ss :setlocal spell!<cr>

"Column 80
set colorcolumn=80

"persistant undo
set undofile

"%% in command line expands to the full path
cabbr <expr> %% expand('%:p:h')

" LaTeX-things
set grepprg=grep\ -nH\ $*
let g:tex_flavor = "latex"
let g:Tex_DefaultTargetFormat = "pdf"
set iskeyword+=:

"Vim-Plug (plugin manager)
call plug#begin('~/.vim/plugged')

"Better status-bar
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

"Agda in vim
Plug 'derekelkins/agda-vim'
"Use my own version
"Plug 'git@gitlab.com:the-real-doctor-phil/personal-agda-vim.git'

"Rust in vim
Plug 'rust-lang/rust.vim'

"Snippets
Plug 'SirVer/ultisnips'
"Plug 'honza/vim-snippets'

"Elm
Plug 'elmcast/elm-vim'

"Purescript
Plug 'raichoo/purescript-vim'

"End of plugins
call plug#end()

"Configuring vim-airline
set laststatus=2
let g:airline_theme='bubblegum'

"vim-airline symbols
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif

" unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.crypt = '🔒'
let g:airline_symbols.linenr = '␊'
let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.maxlinenr = '☰'
let g:airline_symbols.maxlinenr = ''
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.spell = 'Ꞩ'
let g:airline_symbols.notexists = '∄'
let g:airline_symbols.whitespace = 'Ξ'

"Snippet-settings
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"
let g:UltiSnipsEditSplit="vertical"
let g:UltiSnipsListSnippets='<c-b>'
