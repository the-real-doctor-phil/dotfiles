# ----------------------
# History settings
# ----------------------
HISTFILE=~/.zsh_hist
HISTSIZE=1000
SAVEHIST=1000

# ----------------------
# Variables
# ----------------------
export BROWSER="firefox"
export EDITOR="vim"
export VISUAL="vim"
export XMLLINT_INDENT="	"
export PAGER=/bin/less
export PATH="~/bin:/home/simon/.cabal/bin/:$PATH"
PATH="`ruby -e 'print Gem.user_dir'`/bin:$PATH"

export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"

export ANDROID_HOME="$HOME/.Android/Sdk"

# ----------------------
# Aliases
# ----------------------

alias grep='grep --color=auto'
alias :q='exit'
alias ls='ls --color -h'
alias diff='colordiff'

alias uzbl='uzbl-browser'
alias df='df -h'
alias du='du -c -h'
alias ping='ping -c 4'
alias nano='nano -A -S -i -k -m'
alias ydl='youtube-dl -f 45/22/44/43/18'

alias pacman='sudo pacman'
alias reboot='sudo reboot'
alias halt='sudo halt'
alias netcfg='sudo netcfg'
alias sudoe='sudo -e'
alias sudiff='sudo gvimdiff'
alias xampp='sudo xampp'
alias systemctl='sudo systemctl'
alias screencast='ffmpeg -f alsa -ac 2 -i pulse -f x11grab -r 30 -s hd1080 -i :0.0+nomouse -acodec pcm_s16le -vcodec libx264 -vpre lossless_ultrafast -threads 0 latest.mkv'

# ----------------------
# Key bindings
# ----------------------
bindkey -v

typeset -A key
key[Home]=${terminfo[khome]}
key[End]=${terminfo[kend]}
key[Insert]=${terminfo[kich1]}
key[Delete]=${terminfo[kdch1]}
key[Up]=${terminfo[kcuu1]}
key[Down]=${terminfo[kcud1]}
key[Left]=${terminfo[kcub1]}
key[Right]=${terminfo[kcuf1]}
key[PageUp]=${terminfo[kpp]}
key[PageDown]=${terminfo[knp]}

[[ -n "${key[Home]}"    ]]  && bindkey  "${key[Home]}"		beginning-of-line
[[ -n "${key[End]}"     ]]  && bindkey  "${key[End]}"     	end-of-line
[[ -n "${key[Insert]}"  ]]  && bindkey  "${key[Insert]}"  	overwrite-mode
[[ -n "${key[Delete]}"  ]]  && bindkey  "${key[Delete]}"  	delete-char
[[ -n "${key[Up]}"		]]	&& bindkey	"${key[Up]}"		history-beginning-search-backward
[[ -n "${key[Down]}"	]]	&& bindkey	"${key[Down]}"		history-beginning-search-forward
[[ -n "${key[Left]}"    ]]  && bindkey  "${key[Left]}"    	backward-char
[[ -n "${key[Right]}"   ]]  && bindkey  "${key[Right]}"   	forward-char

# ----------------------
# Prompt
# ----------------------
setopt prompt_subst
autoload -U colors && colors

PROMPT='$(pwd_string)${(e)PR_FILLBAR}$(git_string)
%#'

function precmd {
	#The number of characters we can use
	local TERMWIDTH
	(( TERMWIDTH = ${COLUMNS} ))

	pwdsize=${#${(%):-"[ %n@%M %~ ]"}}
	gitsize=${#${(%):-"$(_num_untracked) $(_num_uncommitted) $(_num_unadded) $(_branch)"}}
	
	local space=" "	
	PR_FILLBAR="\${(l.(($TERMWIDTH - ($pwdsize + $gitsize))).. .)}"
}

function _branch() {
	if $(git branch &> /dev/null); then
		echo -n "$(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/')"
	fi
}

#Files not added
function _num_untracked() {
	expr `git status --porcelain 2>/dev/null| grep "^??" | wc -l`
}

#Files that are added
function _num_uncommitted() {
	expr $(git status --porcelain 2>/dev/null| grep "^A " | wc -l)
}

#Files that are modified since last add
function _num_unadded() {
	expr $(git status --porcelain 2>/dev/null| grep "^.M" | wc -l)
}

function git_string() {
	if $(git branch &> /dev/null); then
		echo -n "%{$fg[red]%}$(_num_untracked) %{$fg[yellow]%}$(_num_uncommitted) %{$fg[green]%}$(_num_unadded) %{$fg[blue]%}$(_branch)%{$reset_color%}"
	fi
}

function pwd_string() {
	if [[ $UID == 0 ]]; then
		local NAME="%B%U%{$fg[red]%}%n%u%b"
	else
		local NAME="%{$fg[green]%}%n"
	fi
	echo -n "[ $NAME%{$reset_color%}@%M %{$fg_bold[cyan]%}%~%{$reset_color%} ]"
}

# ----------------------
# Colors
# ----------------------
if [ "$TERM" = "linux" ]; then
	echo -en "\e]P0073642" #black
	echo -en "\e]P8002b36" #brblack
	echo -en "\e]P1dc322f" #red
	echo -en "\e]P9cb4b16" #brred
	echo -en "\e]P2859900" #green
	echo -en "\e]PA586e75" #brgreen
	echo -en "\e]P3b58900" #yellow
	echo -en "\e]PB657b83" #bryellow
	echo -en "\e]P4268bd2" #blue
	echo -en "\e]PC839496" #brblue
	echo -en "\e]P5d33682" #magenta
	echo -en "\e]PD6c71c4" #brmagenta
	echo -en "\e]P62aa198" #cyan
	echo -en "\e]PE93a1a1" #brcyan
	echo -en "\e]P7eee8d5" #white
	echo -en "\e]PFfdf6e3" #brwhite
	clear #for background artifacting
fi

# ----------------------
# Completion
# ----------------------
zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle ':completion:*' completions 1
zstyle ':completion:*' expand prefix suffix
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' glob 1
zstyle ':completion:*' group-name ''
zstyle ':completion:*' ignore-parents parent pwd directory
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' list-suffixes true
zstyle ':completion:*' matcher-list '' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]} r:|[._-]=* r:|=*' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]} r:|[._-]=** r:|=** l:|=*'
zstyle ':completion:*' max-errors 2
zstyle ':completion:*' menu select=4
zstyle ':completion:*' preserve-prefix '//[^/]##/'
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' substitute 1
zstyle ':completion:*' verbose true
zstyle :compinstall filename '/home/simon/.zshrc'

autoload -Uz compinit && compinit

# ----------------------
# Misc settings
# ----------------------
setopt appendhistory autocd extendedglob nomatch
unsetopt beep notify


# ----------------------
# Auto login
# ----------------------
[[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && startx
